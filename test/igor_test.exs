# Copyright 2019-2020 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule IgorTest do
  use ExUnit.Case
  doctest Igor

  test "initializes" do
    {:ok, %{bot: bot}} =
      Igor.init(
        bot_name: "igor",
        mxid: "@igor:example.com",
        aka: ["bot"],
        command_prefixes: ["!"],
        homeserver_url: "https://example.com/",
        access_token: "abcdefg",
        make_client: fn _, _, _ -> nil end
      )

    # check that the command prefix is calculated correctly
    assert Regex.run(bot.command_prefix, "igor foo")
    assert Regex.run(bot.command_prefix, "igor:  foo")
    assert Regex.run(bot.command_prefix, "@igor:example.com foo")
    assert Regex.run(bot.command_prefix, "@igor:example.com: foo")
    assert Regex.run(bot.command_prefix, "bot foo")
    assert Regex.run(bot.command_prefix, "!foo")
    assert !Regex.run(bot.command_prefix, "igor")
    assert !Regex.run(bot.command_prefix, "igorfoo")
    assert !Regex.run(bot.command_prefix, "~foo")
  end

  test "send message" do
    client =
      DummyClient.create(
        "@igor:example.com",
        self(),
        [
          {
            %Polyjuice.Client.Endpoint.PutRoomsSend{
              room: "!room_id",
              txn_id: "txn_id",
              event_type: "m.room.message",
              message: %{
                "body" => "foo"
              }
            },
            {:ok, "$event_id"}
          },
          {
            %Polyjuice.Client.Endpoint.PutRoomsSend{
              room: "!room_id",
              txn_id: "txn_id",
              event_type: "m.room.message",
              message: %{
                "body" => "foo"
              }
            },
            {:ok, "$event_id"}
          },
          {
            %Polyjuice.Client.Endpoint.PutRoomsSend{
              room: "!room_id",
              txn_id: "txn_id",
              event_type: "m.room.message",
              message: %{
                "body" => "foo",
                "msgtype" => "m.notice"
              }
            },
            {:ok, "$event_id"}
          }
        ],
        %{}
      )

    igor = %Igor{
      client: client
    }

    Igor.send(%Igor.Message{content: %{"body" => "foo"}}, "!room_id", igor)
    Igor.send(%{"body" => "foo"}, "!room_id", igor)
    Igor.send("foo", "!room_id", igor)

    assert_raise ArgumentError, fn ->
      Igor.send(igor, "!room_id", igor)
    end

    {:ok, _} = DummyClient.await(client)
  end

  test "react" do
    client =
      DummyClient.create(
        "@igor:example.com",
        self(),
        [
          {
            %Polyjuice.Client.Endpoint.PutRoomsSend{
              room: "!room_id",
              txn_id: "txn_id",
              event_type: "m.reaction",
              message: %{
                "m.relates_to" => %{
                  "rel_type" => "m.annotation",
                  "event_id" => "$event1",
                  "key" => ":)"
                }
              }
            },
            {:ok, "$event_id"}
          }
        ],
        %{}
      )

    igor = %Igor{
      client: client
    }

    Igor.react(
      ":)",
      %Igor.Message{
        room: "!room_id",
        event_id: "$event1",
        content: %{"body" => "foo"}
      },
      igor
    )

    {:ok, _} = DummyClient.await(client)
  end

  test "accepts/rejects invites" do
    client =
      DummyClient.create(
        "@igor:example.com",
        self(),
        [
          {
            %Polyjuice.Client.Endpoint.PostJoin{room: "!room1"},
            :ok
          },
          {
            %Polyjuice.Client.Endpoint.PostRoomsLeave{room: "!room2"},
            :ok
          },
          {
            %Polyjuice.Client.Endpoint.PostRoomsForget{room: "!room2"},
            :ok
          }
        ],
        %{}
      )

    {:ok, pid} =
      Igor.start_link(
        client: client,
        bot_name: "igor",
        mxid: "@igor:example.com",
        accept_invites: ["@master:example.com"]
      )

    send(pid, {:polyjuice_client, :invite, {"!room1", "@master:example.com", %{}}})
    send(pid, {:polyjuice_client, :invite, {"!room2", "@user:example.com", %{}}})

    {:ok, _} = DummyClient.await(client)

    Process.unlink(pid)
    Process.exit(pid, :normal)
  end

  test "html_to_text ignores non-matrix.to URLs" do
    assert Igor.html_to_text("<a href=\"https://uhoreg.ca\">uhoreg.ca</a>") == "uhoreg.ca"
  end
end

# Copyright 2019-2020 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule DummyClient do
  require ExUnit.Assertions
  import ExUnit.Assertions

  defstruct [:agent_pid, :ignore_read_receipts, :user_id]

  def create(user_id, handler, requests, messages, opts \\ []) do
    {:ok, agent_pid} = Agent.start(fn -> requests end)

    Enum.each(
      messages,
      fn {room, messagelist} ->
        Enum.each(
          messagelist,
          &Polyjuice.Client.Handler.handle(handler, :message, {room, &1})
        )
      end
    )

    %__MODULE__{
      agent_pid: agent_pid,
      ignore_read_receipts: Keyword.get(opts, :ignore_read_receipts, false),
      user_id: user_id
    }
  end

  def await(%__MODULE__{agent_pid: agent_pid}, timeout \\ 5000) do
    ref = Process.monitor(agent_pid)

    receive do
      {:DOWN, ^ref, :process, ^agent_pid, reason} ->
        {:ok, reason}
    after
      timeout ->
        :timeout
    end
  end

  def done?(%__MODULE__{agent_pid: agent_pid}) do
    not Process.alive?(agent_pid)
  end

  defimpl Polyjuice.Client.API do
    def call(%{agent_pid: agent_pid, ignore_read_receipts: ignore_read_receipts}, endpoint) do
      if !ignore_read_receipts or !match?(%Polyjuice.Client.Endpoint.PostRoomsReceipt{}, endpoint) do
        {{expected_request, response}, empty} =
          Agent.get_and_update(agent_pid, fn [head | tail] -> {{head, tail == []}, tail} end)

        if is_function(expected_request) do
          assert expected_request.(endpoint)
        else
          assert endpoint == expected_request
        end

        if empty do
          Agent.stop(agent_pid)
        end

        response
      else
        :ok
      end
    end

    def room_queue(_client_api, _room_id, func) do
      # this client doesn't have a queue.  Just run the function.
      func.()
    end

    def transaction_id(_), do: "txn_id"

    def get_user_and_device(%{user_id: user_id}) do
      {user_id, "DEVICE"}
    end

    def stop(%{agent_pid: agent_pid}, _, _) do
      if Process.alive?(agent_pid) do
        # make sure we weren't expecting any more requests
        remaining = Agent.get(agent_pid, & &1)
        assert remaining == []

        Agent.stop(agent_pid)
      end

      :ok
    end
  end
end

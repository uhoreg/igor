# Copyright 2019-2020 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Igor.Responder.UptimeTest do
  use ExUnit.Case

  test "help" do
    requests = [
      {
        %Polyjuice.Client.Endpoint.PostRoomsReceipt{
          event_id: "$event_id",
          receipt_type: "m.read",
          room: "!room_id"
        },
        :ok
      },
      {
        fn
          %Polyjuice.Client.Endpoint.PutRoomsSend{
            room: "!room_id",
            txn_id: "txn_id",
            event_type: "m.room.message",
            message: %{
              "body" => _body,
              "msgtype" => "m.notice"
            }
          } ->
            true
        end,
        {:ok, "$event_id"}
      }
    ]

    messages = %{
      "!room_id" => [
        %{
          "sender" => "@alice:example.com",
          "type" => "m.room.message",
          "content" => %{
            "msgtype" => "m.text",
            "body" => "igor: uptime"
          },
          "room_id" => "!room_id",
          "event_id" => "$event_id"
        }
      ]
    }

    {:ok, pid} =
      Igor.start_link(
        make_client: fn handler, _, _ ->
          DummyClient.create("@igor:example.com", handler, requests, messages)
        end,
        bot_name: "igor",
        mxid: "@igor:example.com",
        responders: [
          {Igor.Responder.Uptime, :create, []}
        ]
      )

    client = Igor.get_client(pid)

    {:ok, _} = DummyClient.await(client)
  end
end

# Copyright 2019-2020 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Igor.Responder.SlapTest do
  use ExUnit.Case

  test "slap" do
    requests = [
      {
        %Polyjuice.Client.Endpoint.PutRoomsSend{
          room: "!room_id",
          txn_id: "txn_id",
          event_type: "m.room.message",
          message: %{
            "body" => "slaps @alice:example.com with a large trout",
            "format" => "org.matrix.custom.html",
            "formatted_body" =>
              "slaps <a href=\"https://matrix.to/#/@alice:example.com\">@alice:example.com</a> with a large trout",
            "msgtype" => "m.emote"
          }
        },
        {:ok, "$event_id"}
      },
      {
        %Polyjuice.Client.Endpoint.PutRoomsSend{
          room: "!room_id",
          txn_id: "txn_id",
          event_type: "m.room.message",
          message: %{
            "body" => "slaps @igor:example.com with a large trout",
            "format" => "org.matrix.custom.html",
            "formatted_body" =>
              "slaps <a href=\"https://matrix.to/#/@igor:example.com\">@igor:example.com</a> with a large trout",
            "msgtype" => "m.emote"
          }
        },
        {:ok, "$event_id"}
      },
      {
        %Polyjuice.Client.Endpoint.PutRoomsSend{
          room: "!room_id",
          txn_id: "txn_id",
          event_type: "m.room.message",
          message: %{
            "body" => "refuses to drop its last weapon",
            "msgtype" => "m.emote"
          }
        },
        {:ok, "$event_id"}
      },
      {
        %Polyjuice.Client.Endpoint.PutRoomsSend{
          room: "!room_id",
          txn_id: "txn_id",
          event_type: "m.room.message",
          message: %{
            "body" => "doesn't have paper",
            "msgtype" => "m.emote"
          }
        },
        {:ok, "$event_id"}
      },
      {
        %Polyjuice.Client.Endpoint.PutRoomsSend{
          room: "!room_id",
          txn_id: "txn_id",
          event_type: "m.room.message",
          message: %{
            "body" => "picks up a wet noodle",
            "msgtype" => "m.emote"
          }
        },
        {:ok, "$event_id"}
      },
      {
        %Polyjuice.Client.Endpoint.PutRoomsSend{
          room: "!room_id",
          txn_id: "txn_id",
          event_type: "m.room.message",
          message: %{
            "body" => "drops a large trout",
            "msgtype" => "m.emote"
          }
        },
        {:ok, "$event_id"}
      },
      {
        %Polyjuice.Client.Endpoint.PutRoomsSend{
          room: "!room_id",
          txn_id: "txn_id",
          event_type: "m.room.message",
          message: %{
            "body" => "slaps igor with a wet noodle",
            "msgtype" => "m.emote"
          }
        },
        {:ok, "$event_id"}
      },
      {
        %Polyjuice.Client.Endpoint.PutRoomsSend{
          room: "!room_id",
          txn_id: "txn_id",
          event_type: "m.room.message",
          message: %{
            "body" =>
              "Listen -- strange women lying in ponds distributing swords is no basis for a system of government.  Supreme executive power derives from a mandate from the masses, not from some farcical aquatic ceremony.",
            "msgtype" => "m.notice"
          }
        },
        {:ok, "$event_id"}
      },
      {
        %Polyjuice.Client.Endpoint.PutRoomsSend{
          room: "!room_id",
          txn_id: "txn_id",
          event_type: "m.room.message",
          message: %{
            "body" =>
              ~S("Reverse primary thrust, Marvin." That's what they say to me. "Open airlock number 3, Marvin." "Marvin, can you pick up that piece of paper?" Here I am, brain the size of a planet, and they ask me to pick up a piece of paper.),
            "msgtype" => "m.notice"
          }
        },
        {:ok, "$event_id"}
      }
    ]

    messages = %{
      "!room_id" => [
        %{
          "sender" => "@alice:example.com",
          "type" => "m.room.message",
          "content" => %{
            "msgtype" => "m.text",
            "body" => "!slap"
          },
          "room_id" => "!room_id",
          "event_id" => "$event_id1"
        },
        %{
          "sender" => "@alice:example.com",
          "type" => "m.room.message",
          "content" => %{
            "msgtype" => "m.text",
            "body" => "!slap @igor:example.com"
          },
          "room_id" => "!room_id",
          "event_id" => "$event_id2"
        },
        %{
          "sender" => "@alice:example.com",
          "type" => "m.room.message",
          "content" => %{
            "msgtype" => "m.text",
            "body" => "!drop a large trout"
          },
          "room_id" => "!room_id",
          "event_id" => "$event_id3"
        },
        %{
          "sender" => "@alice:example.com",
          "type" => "m.room.message",
          "content" => %{
            "msgtype" => "m.text",
            "body" => "!drop paper"
          },
          "room_id" => "!room_id",
          "event_id" => "$event_id4"
        },
        %{
          "sender" => "@alice:example.com",
          "type" => "m.room.message",
          "content" => %{
            "msgtype" => "m.text",
            "body" => "!pick up a wet noodle"
          },
          "room_id" => "!room_id",
          "event_id" => "$event_id5"
        },
        %{
          "sender" => "@alice:example.com",
          "type" => "m.room.message",
          "content" => %{
            "msgtype" => "m.text",
            "body" => "!drop a large trout"
          },
          "room_id" => "!room_id",
          "event_id" => "$event_id6"
        },
        %{
          "sender" => "@alice:example.com",
          "type" => "m.room.message",
          "content" => %{
            "msgtype" => "m.text",
            "body" => "!slap igor"
          },
          "room_id" => "!room_id",
          "event_id" => "$event_id7"
        },
        %{
          "sender" => "@alice:example.com",
          "type" => "m.room.message",
          "content" => %{
            "msgtype" => "m.text",
            "body" => "!pick up excalibur"
          },
          "room_id" => "!room_id",
          "event_id" => "$event_id8"
        },
        %{
          "sender" => "@alice:example.com",
          "type" => "m.room.message",
          "content" => %{
            "msgtype" => "m.text",
            "body" => "!pick up paper"
          },
          "room_id" => "!room_id",
          "event_id" => "$event_id9"
        }
      ]
    }

    {:ok, pid} =
      Igor.start_link(
        make_client: fn handler, _, _ ->
          DummyClient.create("@igor:example.com", handler, requests, messages,
            ignore_read_receipts: true
          )
        end,
        bot_name: "igor",
        mxid: "@igor:example.com",
        command_prefixes: ["!"],
        responders: [
          {Igor.Responder.Slap, :create, []}
        ]
      )

    client = Igor.get_client(pid)

    {:ok, _} = DummyClient.await(client)
  end

  test "help slap" do
    requests = [
      {
        %Polyjuice.Client.Endpoint.PostRoomsReceipt{
          event_id: "$event_id",
          receipt_type: "m.read",
          room: "!room_id"
        },
        :ok
      },
      {
        %Polyjuice.Client.Endpoint.PutRoomsSend{
          room: "!room_id",
          txn_id: "txn_id",
          event_type: "m.room.message",
          message: %{
            "body" =>
              "igor help [command...] - display help\nigor drop <weapon> - drop a weapon\nigor pick up <weapon> - pick up a weapon to use for slapping\nigor slap [user] - slap a user",
            "msgtype" => "m.notice"
          }
        },
        {:ok, "$event_id"}
      }
    ]

    messages = %{
      "!room_id" => [
        %{
          "sender" => "@alice:example.com",
          "type" => "m.room.message",
          "content" => %{
            "msgtype" => "m.text",
            "body" => "igor: help"
          },
          "room_id" => "!room_id",
          "event_id" => "$event_id"
        }
      ]
    }

    {:ok, pid} =
      Igor.start_link(
        make_client: fn handler, _, _ ->
          DummyClient.create("@igor:example.com", handler, requests, messages)
        end,
        bot_name: "igor",
        mxid: "@igor:example.com",
        responders: [
          Igor.Responder.Help,
          {Igor.Responder.Slap, :create, []}
        ]
      )

    client = Igor.get_client(pid)

    {:ok, _} = DummyClient.await(client)
  end
end

0.3.0
=====

Breaking changes:

- Uses Polyjuice Client v0.4.0, which includes breaking changes from the
  previous version.  If you are not creating your own client, this change
  should not affect you.

Features:

- New responder for converting time stamps to a date string.
- mxid parameter is now optional when creating the Igor instance, if the
  underlying Polyjuice.Client knows the bot's Matrix ID
- The Igor client struct now implements the `Polyjuice.Client.API`, so can
  be used by functions that use that protocol.  It will use the underlying
  Polyjuice client.

Bug fixes:

- Fix parsing of `<a>` tags

0.2.1
=====

Features:

- Allow specifying additional options for Polyjuice Client.
- Allow using newer version of html5ever for compatibility with newer OTP
  versions.

0.2.0
=====

Breaking changes:

- Uses Polyjuice Client v0.3.0, which includes breaking changes from the
  previous version.  If you are not creating your own client, this change
  should not affect you.
  - new configuration options have been added for creating clients

Features:

- Improved spec compliance.
- Add configuration to accept or reject invites.

0.1.0
=====

Initial release.

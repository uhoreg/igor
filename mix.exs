defmodule Igor.MixProject do
  use Mix.Project

  def project do
    [
      app: :igor,
      description: "Bot framework for Matrix.org",
      version: "0.3.0",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      elixirc_paths: elixirc_paths(Mix.env()),

      # Docs
      name: "Igor",
      source_url: "https://gitlab.com/uhoreg/igor",
      # homepage_url: "https://www.uhoreg.ca/programming/matrix/igor",
      docs: [
        # The main page in the docs
        main: "readme",
        # logo: "path/to/logo.png",
        extras: ["README.md"]
      ],
      package: [
        maintainers: ["Hubert Chathi"],
        licenses: ["Apache-2.0"],
        links: %{
          "Source" => "https://gitlab.com/uhoreg/igor"
        }
      ],

      # Tests
      test_coverage: [
        summary: [threshold: false]
      ]
    ]
  end

  def application do
    [
      extra_applications: [:logger, :hackney]
    ]
  end

  defp deps do
    [
      {:ex_doc, "~> 0.21", only: :dev, runtime: false},
      {:hackney, "~> 1.12"},
      {:html5ever, ">= 0.7.0 and < 0.9.0"},
      {:polyjuice_client, "~> 0.4.0"}
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]
end
